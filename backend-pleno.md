#Desenvolvedor Ruby on Rails

Estamos contratando desenvolvedor full-time pleno, com experiência em Ruby on Rails para integrar nossa equipe.

Responsabilidades e habilidades necessárias:
- Experiência com desenvolvimento Ruby on Rails(incluindo design patterns, TDD).
- Garantir a qualidade do código entregue.
- Experiência com times auto-gerenciáveis e metodologia ágil. 
- Participar da comunidade e projetos open source é diferencial.
- Comprometimento com o projeto e prazos combinados.


##Habilidades
* Necessário ter experiência com: Ruby on Rails, APIs REST, TDD/BDD, GIT, PostgreSQL, Inglês. 
* Desejável: Javascript, Reactjs/Angularjs/Emberjs, Elasticsearch, RSpec.
