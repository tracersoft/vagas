Vagas disponíveis
-----------------

* **Desenvolvedor com experiência VTEX(Front & Back)**
* [Desenvolvedor Ruby on Rails](https://github.com/tracersoft/vagas/blob/master/backend-pleno.md)
* [Desenvolvedor Front-end](https://github.com/tracersoft/vagas/blob/master/frontend-pleno.md)

Sobre nós
---------

Somos uma empresa com ambiente descontraído, horários flexíveis. Nossas decisões são tomadas em equipe e estamos sempre abertos às novas tecnologias e tendências. Você trabalhará em uma equipe pequena, multi-disciplinar e auto-gerenciável.

Nosso stack é moderno e utilizamos diversas ferramentas/tecnologias de desenvolvimento como: Heroku, CodeCircle, AWS(EC2, RDS, etc), entre outras.

Somos perfeccionistas com qualidade de código e padrões de design, utilizamos TDD, Pull Requests, etc. Nossos desenvolvedores precisam se incomodar em escrever códigos de má qualidade.

Faixas salariais(CLT)
-----------

  - Estágio: 1000 - 1600
  - Júnior: 1800 - 2800
  - Pleno: 3000 - 4600
  - Sênior: 5000+

#####Interessados entrar em contato com marcelo@tracersoft.com.br enviando seu github, linkedin e sua pretensão salarial.
