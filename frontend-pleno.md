#Desenvolvedor Front-end

Estamos contratando desenvolvedor frontend full-time pleno, com experiência em Javascript(ES6) e Sass/PostCSS para integrar nossa equipe.


Responsabilidades e habilidades necessárias:
- Necessário conhecer o básico de Rails para poder realizar a integração com o backend(Principalmente via API REST).
- Garantir a qualidade do código entregue.
- Experiência com times auto-gerenciáveis e metodologia ágil. 
- Participar da comunidade e projetos open source é diferencial.
- Comprometimento com o projeto e prazos combinados.


##Habilidades

* Necessário ter experiência com:  Javascript(ES6), Sass/PostCSS, React/Angular/Ember/Outro, Gulp/Grunt, TDD, REST, Scrum/XP, UX/UI, Inglês.
* Desejável: Ruby on Rails, Node.js.
